/* Kadane - Get the largest sum of a subarray
 *
 * Complexity (Time): O(n)
 *
 * Complexity (Space): O(n)
 *
 * Problems solved with this:
 *    Saldo de gols (SALDO)
 */

int v[MAX];

int kadane() {

  // Maximum so far (msf), Maximum ending here (meh).
  int msf = -0x3f3f3f3f, meh = 0;

  // (*) Not necessary unless the problem asks for the bounds of the resulting
  // subarray.
  int start = 0, end = 0, s = 0;

  for (int i = 0; i < n; ++i) {
    // Update maximum ending here
    meh += v[i];

    // Result gets updated if maximum ending here is the best so far.
    if (msf < meh) {
      msf = meh;
      start = s; // *
      end = i;   // *
    }

    // If meh is negative, then it's certainly not better than stopping here.
    if (meh < 0) {
      meh = 0;
      s = i + 1; // *
    }
  }

  return msf;
}
