/* Longest Palindromic Subsequence
 *
 * Complexity (Time): O(n^2)
 *
 * Complexity (Space): O(n^2)
 *
 * Problems solved with this:
 *    Palindromo (SBC 2015)
 */

int dp[MAX][MAX];

int lps(string s) {
  memset(dp, 0, sizeof(dp));

  // Subsequences with size 1 are palindromes
  for (int i = 0; i < s.size(); ++i)
    dp[i][i] = 1;

  for (int l = 2; l <= s.size(); ++l) {
    for (int i = 0; i < s.size() - l + 1; ++i) {
      // i is the beginning of the subsequence and j is the end
      int j = i + l - 1;

      // If the subsequence has size 2 and both of the elements (letters) are
      // equal, then it is a palidrome of size 2.
      if (s[i] == s[j] && l == 2)
        dp[i][j] = 2;

      // If the extremities are equal, then the new longest palindrome
      // consists of appending the corners to the best result so far.
      else if (s[i] == s[j])
        dp[i][j] = dp[i + 1][j - 1] + 2;

      // Otherwise the longest palidrome in the middle gets carried on to the
      // current best result.
      else
        dp[i][j] = max(dp[i][j - 1], dp[i + 1][j]);
    }
  }

  return dp[0][s.size() - 1];
}
