/* About grundy numbers:
 *
 * The grundy number of a game (instantiated) is the MEX (minimum excluded) of
 * the set of the grundy numbers of it's sons (possible plays).
 *
 * If a game can be split in other N independent games, it's grundy number is
 * the cumulative XOR of the N game's grundy numbers.
 *
 * --Sprague-Grundy Theorem--
 * If the grundy number of the current state is 0, the player ready to play is
 * losing, else, it's winning.
 *
 * Grundy numbers are only applicable in impartial games, which are defined as
 * "games in which the allowable moves depend only on the position and not on
 * which of the two players is currently moving, and where the payoffs are
 * symmetric."
 *
 * Complexity (Time): O(n^2)  ~~depends on the game
 *
 * Complexity (Space): O(n)   ~~depends on the game
 *
 * Problems solved with this:
 *    Jogo da Velha (URI1130)
 */

const int MAX = 11234;
int n;
// dp timestamp
int TS = 1;

char g[MAX];
int mem[MAX];
int timestamp[MAX];

int solve(int m) {
  // base cases - very important
  // 0 for terminal cases
  if (m <= 0)
    return 0;
  if (m <= 3)
    return 1;
  if (timestamp[m] == TS)
    return mem[m];

  // allocates auxiliar structure to calculate mex
  // if it causes runtime error or it's too slow, (shouldn't)
  // you may code a iterative version, so a global array is enough.
  bool *mex = (bool *)calloc(m, sizeof(bool));

  // for every play possible in the current game
  for (int i = 1; i <= m; i++) {
    // in some problems, a play split the game in other two independent games
    // so XOR them
    int son1 = max(0, i - 3);
    int son2 = max(m - (i + 2), 0);
    int g1 = solve(son1);
    int g2 = solve(son2);
    g1 ^= g2;

    // insert this play in the mex set
    mex[g1] = 1;
  }

  // calculates mex of the plays set
  int g = 0;
  while (mex[g])
    g++;

  // g is the grundy number of this state
  // if its 0, its a losing state for the player
  // else its winning
  mem[m] = g;
  timestamp[m] = TS;
  return g;
}
