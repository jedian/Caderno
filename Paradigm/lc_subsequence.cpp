/* Longest Common Subsequence
 *
 * Complexity (Time): O(nm)
 *
 * Complexity (Space): O(nm)
 *
 * Problems solved with this:
 *    Ajude Chaves
 */

int dp[MAX][MAX];

int lcs(string a, string b) {
  for (int i = 0; i <= a.size(); ++i) {
    for (int j = 0; j <= b.size(); ++j) {
      // Left and upper edges of dp matrix should be zero, thus defining that
      // the base of the problem is the LCS of two empty string (zero)
      if (!i || !j)
        dp[i][j] = 0;

      // If the letters are equal, then register result in matrix considering
      // the substrings already solved as a subproblem in the past
      else if (a[i - 1] == b[j - 1])
        dp[i][j] = dp[i - 1][j - 1] + 1;

      // Otherwise carry the results from the past subproblems to the current
      // matrix position (maximum result for obtaining the longest subsequence)
      else
        dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
    }
  }

  return dp[a.size()][b.size()];
}
