/* Subset Sum
 *
 * Complexity (Time): O(n*sum)
 *
 * Complexity (Space): O(sum)
 *
 * Problems solved with this:
 *    ?
 */

int arr[MAX];
bool dp[2][SUM];

// ======================================================
// Returns true if there is a subset of arr which sums up to "sum"

bool subset_sum(int n, int sum) {
  for (int i = 0; i <= n; ++i)
    for (int j = 0; j <= sum; ++j)
      if (!j)
        dp[i % 2][j] = true;
      else if (!i)
        dp[i % 2][j] = false;
      else if (arr[i - 1] <= j)
        dp[i % 2][j] = dp[(i + 1) % 2][j - arr[i - 1]] || dp[(i + 1) % 2][j];
      else
        dp[i % 2][j] = dp[(i + 1) % 2][j];

  return dp[n % 2][sum];
}

// =========== Print every resulting subset =============
// Complexity (Space): O(n*sum)

void print_subsets(int i, int sum, vector<int> &p) {
  if (!i && sum && dp[0][sum]) {
    p.push_back(arr[i]);
    for (auto x : p)
      printf("%d ", x);
    printf("\n");

    return;
  }

  if (!i && !sum) {
    for (auto x : p)
      printf("%d ", x);
    printf("\n");

    return;
  }

  if (dp[i - 1][sum]) {
    vector<int> b = p;
    print_subsets(i - 1, sum, b);
  }

  if (sum >= arr[i] && dp[i - 1][sum - arr[i]]) {
    p.push_back(arr[i]);
    print_subsets(i - 1, sum - arr[i], p);
  }
}

void solve(int n, int sum) {
  if (!n || sum < 0)
    return;

  for (int i = 0; i < n; ++i)
    dp[i][0] = true;

  if (arr[0] <= sum)
    dp[0][arr[0]] = true;

  for (int i = 1; i < n; ++i)
    for (int j = 0; j < sum + 1; ++j)
      if (arr[i] <= j)
        dp[i][j] = dp[i - 1][j] || dp[i - 1][j - arr[i]];
      else
        dp[i][j] = dp[i - 1][j];

  if (!dp[n - 1][sum])
    return;

  vector<int> p;
  print_subsets(n - 1, sum, p);
}
