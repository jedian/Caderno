/* Ternary Search - find minimum or maximum of a unimodal function
 *
 * Complexity (Time): O(log n)
 *
 * Complexity (Space): O(1)
 *
 * Problems solved with this:
 *    A Caminhada da Vergonha de Cersei
 *    Desafio PoGro
 */

#define EPS 0.000001

// Unimodal function
double f(double x) {
  return x * x //, for example
}

// l and r are the bounds of f.
double ternary_search(double l, double r) {
  double rt, lt;

  for (int i = 0; i < 500; ++i) {
    if (fabs(r - l) < EPS)
      return (l + r) / 2.0;

    // Left third
    lt = l + (r - l) / 3.0;
    // Right third
    rt = r - (r - l) / 3.0;

    // < | minimum of f
    // > | maximum of f
    if (f(lt) < f(rt))
      l = lt;
    else
      r = rt;
  }

  return (l + r) / 2.0;
}
