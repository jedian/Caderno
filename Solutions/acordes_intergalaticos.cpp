#include <bits/stdc++.h>

using namespace std;
const int MAX = 101010;

typedef struct elem {
  int v[10];

  elem() {
    for (int i = 0; i < 9; ++i)
      v[i] = 0;
  }

  elem operator+(elem a) {
    elem aux;
    for (int i = 0; i < 9; ++i)
      aux.v[i] = a.v[i] + v[i];

    return aux;
  }

  int grt() {
    int ans = 0;
    for (int i = 0; i < 9; ++i)
      if (v[i] >= v[ans])
        ans = i;

    return ans;
  }

  void shift(int x) {
    int aux[10];
    for (int i = 0; i < 9; ++i)
      aux[i] = v[i];
    for (int i = 0; i < 9; ++i)
      v[i] = aux[(i - x + 9) % 9];
  }
} elem;

int N, lazy[4 * MAX];
elem tree[4 * MAX];

void build_tree(int node = 1, int a = 0, int b = N - 1) {
  if (a > b)
    return;              // out of range

  if (a == b) {          // is leaf
    elem aux;
    aux.v[1] = 1;
    tree[node] = aux;

    return;
  }

  build_tree(node * 2, a, (a + b) / 2);
  build_tree(node * 2 + 1, 1 + (a + b) / 2, b);

  tree[node] = tree[node * 2] + tree[node * 2 + 1]; // segment info
}

void update_tree(int i, int j, int val, int node = 1, int a = 0, int b = N - 1) {
  if (lazy[node] != 0) {
    tree[node].shift(lazy[node]);

    if (a != b) {
      lazy[node * 2] = (lazy[node * 2] + lazy[node]) % 9;
      lazy[node * 2 + 1] = (lazy[node * 2 + 1] + lazy[node]) % 9;
    }

    lazy[node] = 0;
  }

  if (a > b || a > j || b < i) // out of range
    return;

  if (a >= i && b <= j) { // segment fully within range
    tree[node].shift(val);

    if (a != b) {
      lazy[node * 2] = (lazy[node * 2] + val) % 9;
      lazy[node * 2 + 1] = (lazy[node * 2 + 1] + val) % 9;
    }

    return;
  }

  update_tree(i, j, val, node * 2,  a, (a + b) / 2);
  update_tree(i, j, val, node * 2 + 1, 1 + (a + b) / 2, b);

  tree[node] = tree[node * 2] + tree[node * 2 + 1]; // update parents
}

elem query_tree(int i, int j, int node = 1, int a = 0, int b = N - 1) {
  if (a > b || a > j || b < i) {
    elem aux;
    return aux;            // out of range
  }

  if (lazy[node] != 0) { // this node needs to be updated
    tree[node].shift(lazy[node]);

    if (a != b) { // if not leaf, propagates changes
      lazy[node * 2] = (lazy[node * 2] + lazy[node]) % 9;
      lazy[node * 2 + 1] = (lazy[node * 2 + 1] + lazy[node]) % 9;
    }

    lazy[node] = 0;
  }

  if (a >= i && b <= j)
    return tree[node];

  elem q1 = query_tree(i, j, node * 2, a, (a + b) / 2);
  elem q2 = query_tree(i, j, node * 2 + 1, 1 + (a + b) / 2, b);
  return q1 + q2;
}

int main() {
  int q, a, b;
  scanf("%d %d", &N, &q);
  build_tree();

  for (int i = 0; i < q; ++i) {
    scanf("%d %d", &a, &b);

    elem aux = query_tree(a, b);
    int val = aux.grt();
    update_tree(a, b, val);
  }

  for (int j = 0; j < N; ++j)
    printf("%d\n", query_tree(j, j).grt());
}
