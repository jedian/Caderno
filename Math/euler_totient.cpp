/* Euler's totient function
*  Returns the number of coprimes less than n (not divisors of n)
*  Example: et(8) = |{1, 3, 5, 7}| = 4
* 			et(7) = |{1, 2, 3, 4, 5, 6}| = 6
*
*  Complexity: ?? (less than O(sqrt(n)))
*/

vector<int> primes; // to be filled with eratosthenes sieve

typedef long long ll;

ll euler_totient(ll n) {
  ll res = n;

  for (auto p : primes) {
    if (p * p > n)
      break;

    if (!(n % p)) {
      while (!(n % p))
        n /= p;

      res -= res / p;
    }
  }

  if (n > 1)
    res -= res / n;

  return res;
}
