/* Inclusion Exclusion Principle
 *
 * Complexity (Time): O(n)
 *
 * Complexity (Space): O(n^2)
 *
 * Problems solved with this:
 *   CarelessSecretary
 */

// |A u B u C| = (|A| + |B| + |C|) - (|A ^ B| + |A ^ C| + |B ^ C|) +
//   (|A ^ B ^ C|)

#define MOD 1000000007

int fact[MAX], C[MAX][MAX];

// SAMPLE PROBLEM (CarelessSecretary):
//   Find number of permutations of a n-element array such that there's no
//   cycle of length 1 among the first k elements
//   Example (n = 3, k = 2):
//     1 2 3
//     1 3 2
//     2 1 3 -
//     2 3 1 -
//     3 1 2 -
//     3 2 1

int inclusion_exclusion(int n, int k) {

  // Factorial up to n
  fact[0] = 1;
  for (int i = 1; i <= n; ++i)
    fact[i] = (fact[i - 1] * i) % MOD;

  // Combinations
  for (int i = 0; i <= k; ++i) {
    // C(n, 0) = C(n, n) = 1
    C[i][0] = 1;

    // C(n, k) = C(n - 1, k - 1) + C(n - 1, k)
    for (int j = 0; j <= i; ++j)
      C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % MOD;
  }

  int ans = fact[n];
  for (int i = 1; i <= k; ++i)
    if (i % 2)
      ans = (MOD + ans - (C[k][i] * fact[n - i]) % MOD) % MOD; // Exclusion
    else
      ans = (ans + C[k][i] * fact[n - i]) % MOD; // Inclusion

  // ans = n! - (C(k, 1) * (n - 1)!) + (C(k, 2) * (n - 2)!)...

  // EXPLANATION: In the beginning, n! permutations are considered. In the
  // second step, one element, out of the first k, is fixed (the element with
  // cycle length = 1) and for every case, out of the C(k, 1), there are
  // (n - 1)! possible configurations for the remaining elements. Along the
  // first step, some permutations with more than 1 cyclic element are
  // included, they need to be excluded in the next steps.

  return ans;
}
