/* Fast matrix pow
 *
 * Complexity (Time): O(k^3 log n)
 *
 * Complexity (Space): O(k^2)
 *
 * Problems solved with this:
 *    Onibus
 */

// This algorithm is used to solve recurrences such as:
//   f(n) = x1 * f(n - 1) + x2 * f(n - 1) + ... + xk * f(n - k)
//
// It works by defining this recurrence as a linear combination,
// for example (k = 2):
//   f(n) = [x1  x2] [f(n - 1)]
//                   [f(n - 2)]
//
// It can be rewriten as:
//   [  f(n)  ] = [x1  x2] [f(n - 1)]
//   [f(n - 1)]   [ 1   0] [f(n - 2)]
//
// And that is solved by calculating the following matrix power:
//   [x1  x2]^n
//   [ 1   0]

typedef long long ll;

struct mat {
  ll m[3][3];

  // Matrix multiplication - O(k^3)
  mat operator*(mat a) {
    mat aux;
    aux.m[0][0] = ((m[0][0] * a.m[0][0]) % MOD) + ((m[0][1] * a.m[1][0]) % MOD);
    aux.m[0][1] = ((m[0][0] * a.m[0][1]) % MOD) + ((m[0][1] * a.m[1][1]) % MOD);
    aux.m[1][0] = ((m[1][0] * a.m[0][0]) % MOD) + ((m[1][1] * a.m[1][0]) % MOD);
    aux.m[1][1] = ((m[1][0] * a.m[0][1]) % MOD) + ((m[1][1] * a.m[1][1]) % MOD);

    return aux;
  }
};

// Fast exponentiation (can be used with integers as well) - O(log n)
ll mat_pow(mat in, ll n) {
  mat ans, b = in;
  ans.m[0][0] = ans.m[1][1] = 1;
  ans.m[1][0] = ans.m[0][1] = 0;

  while (n) {
    if (n & 1)
      ans = ans * b;

    n >>= 1;
    b = b * b;
  }

  return ans.m[0][0];
}

// Solves f(n) = x * f(n - 1) + y * f(n - 2)
ll solve(ll x, ll y, ll n) {
  mat in;

  in.m[0][0] = x % MOD;
  in.m[0][1] = y % MOD;
  in.m[1][0] = 1;
  in.m[1][1] = 0;

  return mat_pow(in, n) % MOD;
}
