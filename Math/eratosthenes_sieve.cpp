/* Description: This algorithm fills a boolean table with primality
 * information in each cell.
 *
 * Utilities: Primality check in O(1) and prime-sweeping in O(p)
 *
 * Complexity (Time):
 *   Sieve -> O(n log n) (n -> greatest prime)
 *   Query -> O(1)
 *
 * Complexity (Space): O(n)
 *
 * Problems solved with this:
 * CFATORES (Conte os fatores)
*/

const int N = 500000; // if factorizing, limit may be sqrt(N)

bool isprime[N];
vector<int> primes;

void er_sieve() {
  memset(isprime, 1, sizeof isprime);
  isprime[0] = isprime[1] = 0;
  for (int i = 2; i <= N; i++) // limit
    if (isprime[i]) {
      primes.push_back(i);
      for (int j = 2; j * i <= N; j++)
        isprime[i * j] = 0;
    }
}
