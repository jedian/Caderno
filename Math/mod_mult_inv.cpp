/* Modular multiplicative inverse
 *
 * Complexity (Time): O(log m)
 *
 * Complexity (Space): O(1)
 *
 * Problems solved with this:
 *    Jupiter Ataca!
 */

typedef long long ll;

// ========== Fermat's Little Theorem ==========
// Used if m is prime

// (x^y) % m
ll power(ll x, ll y, ll m) {
  ll ans = 1;

  while (y) {
    if (y & 1)
      ans = (ans * x) % m;

    y >>= 1;
    x = (x * x) % m;
  }

  return ans;
}

ll gcd(ll a, ll b) { return (!a) ? b : gcd(b % a, a); }

// Find x where:
//   a*x == 1 mod m
ll mod_inverse(ll a, ll m) {
  return (gcd(a, m) != 1) ? -1 : power(a, m - 2, m);
}

// ========== Extended Euclidean Algorithm ==========
// Used if m and a are coprime

ll gcd_extended(ll a, ll b, ll *x, ll *y) {
  if (!a) {
    *x = 0;
    *y = 1;
    return b;
  }

  ll x1, y1;
  ll g = gcd_extended(b % a, a, &x1, &y1);

  *x = y1 - (b / a) * x1;
  *y = x1;

  return g;
}

// Find x where:
//   a*x == 1 mod m
ll mod_inverse(ll a, ll m) {
  ll x, y;
  ll g = gcd_extended(a, m, &x, &y);

  return (g != 1) ? -1 : (x % m + m) % m;
}
