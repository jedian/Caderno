/* Fast Fourier Transform (FFT)
 *
 * Complexity (Time): O(n log n)
 *
 * Complexity (Space): O(n)
 *
 * Problems solved:
 *   Very Fast Multiplication
 *   Laboratorio de Biotecnologia
 */

#define MAX (1 << 15) + 1
#define LOG2(X) ((8 * sizeof(unsigned long long) - __builtin_clzll((X)) - 1))

typedef complex<double> comp;
typedef unsigned int uint;

int N;

// Returns reversed bits of x
inline uint rev(uint x) {
  x = (((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1));
  x = (((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2));
  x = (((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4));
  x = (((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8));

  return (((x >> 16) | (x << 16)) >> (32 - LOG2(N)));
}

// Perform Discrete Fourier Transform in O(n log n)
// Coefficient representation -> Point-value representation (inv = 1)
// Point-value representation -> Coefficient representation (inv = -1)
void fft(comp *A, int inv) {
  int rr;

  // Swap order to match the end of recursion on the divide and conquer approach
  for (int i = 0; i < N; ++i)
    if (i < (rr = rev(i)))
      swap(A[i], A[rr]);

  for (int s = 1; s < N; s <<= 1) {
    comp wm = comp(cos(inv * M_PI / s), sin(inv * M_PI / s));

    for (int k = 0; k < N; k += (s << 1)) {
      comp w = 1;

      for (int j = 0; j < s; ++j) {
        comp t = w * A[k + j + s];
        comp u = A[k + j];

        A[k + j] = u + t;
        A[k + j + s] = u - t;

        w *= wm;
      }
    }
  }

  if (inv == -1)
    for (int i = 0; i < N; ++i)
      A[i] /= N;
}

// Multiplication of polynomials (c = a * b)
// n1 = size of a
// n2 = size of b
void multiply_poly(comp *a, comp *b, comp *c, int n1, int n2) {
  // N must be a power of 2
  for (N = 1; N < max(n1, n2); N <<= 1)
    ;
  N <<= 1;

  fft(a, 1);
  fft(b, 1);

  for (int j = 0; j <= N; ++j)
    c[j] = a[j] * b[j];

  fft(c, -1);
}
