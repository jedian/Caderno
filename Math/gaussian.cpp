/* Gaussian elimination
 *
 * Complexity (Time): O(n^3) || (O(n * k) : XOR)
 *
 * Complexity (Space): O(n^2) || (O(n) : XOR)
 *
 * Problems solved with this:
 *    Loteria
 *    XMAX
 */

#define EPS 1e-6

int n;
double sol[100];

// Pointer used for more efficient row swap (can be replaced if clarity is
// needed, in this case a new function would have to swap elements one by one)
vector<double> *mat[100];

// Forward elimination (reduce to Echelon Form)
void forward_elim() {
  for (int k = 0; k < n; ++k) {
    int grt = k;

    // Find greatest value at column k (avoid floating-point problems)
    for (int i = k + 1; i < n; ++i)
      if (fabs(mat[i]->at(k)) > mat[grt]->at(k))
        grt = i;

    if (fabs(mat[grt]->at(k)) < EPS) {
      // matrix is singular ((mat[k][n] == 0) ? infite solutions : inconsistent)
      return;
    }

    // Replace k-th row with the one containing the greatest value at column k
    swap(mat[k], mat[grt]);

    // Eliminate values at column k using k-th row as the pivot
    for (int i = k + 1; i < n; ++i) {
      double f = mat[i]->at(k) / mat[k]->at(k);

      for (int j = k + 1; j <= n; ++j)
        (*mat[i])[j] -= mat[k]->at(j) * f;
      (*mat[i])[k] = 0;
    }
  }
}

// Back substitution
void back_sub() {
  for (int i = n - 1; i >= 0; --i) {
    // Solve the i-th equation for the i-th variable
    sol[i] = mat[i]->at(n);

    for (int j = i + 1; j < n; ++j)
      sol[i] -= mat[i]->at(j) * sol[j];

    sol[i] = sol[i] / mat[i]->at(i);
  }
}

void solve() {
  double x;
  scanf("%d", &n);

  for (int i = 0; i < n; ++i) {
    mat[i] = new vector<double>;

    for (int j = 0; j <= n; ++j) {
      scanf("%lf", &x);
      mat[i]->push_back(x);
    }
  }

  forward_elim();
  back_sub();

  for (int i = 0; i < n; ++i)
    printf("%lf\n", sol[i]);
}

// =========================== XOR elimination ============================

// Apply gaussian elimination to array of numbers (defining bitsets as vectors)
void xor_elim(vector<ll> &v) {
  sort(v.begin(), v.end(), greater<ll>());

  ll k = 1;
  while (k <= v[0])
    k <<= 1;
  k >>= 1;

  for (int t = 0; k >= 1; k >>= 1, ++t) {
    int i = t;

    while (i < n && (v[i] & k) == 0)
      i++;

    if (i >= n)
      continue;

    swap(v[t], v[i]);

    for (int j = 0; j < n; ++j)
      if (j != t && (v[j] & k) != 0)
        v[j] = v[j] ^ v[t];
  }
}
