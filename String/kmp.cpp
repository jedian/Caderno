/* KMP - Pattern searching in string algorithm.
 *
 * Complexity (Time): O(n + m)
 *
 * Complexity (Space): O(n + m)
 *
 * Problems solved with this:
 *   Pattern Find (NAJPF)
 *   I Love Strings!!
 */

int table[MAX];
vector<int> occurs;

// Build the table where table[i] = the longest prefix of patt[0..i] which is
// also a sufix of patt[0..i]. Done in O(m)
void preprocess(string patt) {
  int i = 1, len = 0;

  while (i < patt.size()) {
    if (patt[i] == patt[len])
      table[i++] = ++len;
    else if (len)
      len = table[len - 1];
    else
      table[i++] = 0;
  }
}

// Search for occurrences of patt in txt
void search(string patt, string txt) {
  int i = 0, j = 0;

  while (i < txt.size()) {
    if (patt[j] == txt[i])
      i++, j++;

    if (j == patt.size()) {
      // Pattern found at (i - j)
      occurs.push_back(i - j);
      j = table[j - 1];
    } else if (i < txt.size() && patt[j] != txt[i]) {
      if (j)
        j = table[j - 1];
      else
        i++;
    }
  }
}
