/* Convex Hull
 *
 * Complexity (Time): O(n log n)
 *
 * Complexity (Space): O(n)
 *
 * Problems solved with this:
 *    Camadas de Cebola (URI1464)
 */
const int MAX = 1000;
typedef pair<double, double> dd;
vector<dd> V;

bool cmp(dd a, dd b) {
  if (a.first == b.first)
    return a.second < b.second;
  return a.first < b.first;
}

double cross(dd &a, dd &b, dd &c) {
  return (b.first - a.first) * (c.second - a.second) -
         (b.second - a.second) * (c.first - a.first);
}

int Vv[MAX];

// (*) if collinear points are unwanted, change "< 0" to "<= 0"
int convexHull() {
  sort(V.begin(), V.end(), cmp);
  int n = V.size(), k = 0, i, t;
  // select the uppermost part of the convex hull
  for (i = 0; i < n; i++) {
    while (k >= 2 && cross(V[Vv[k - 2]], V[Vv[k - 1]], V[i]) < 0) // (*)
      k--;
    Vv[k++] = i;
  }
  // select the lowermost part
  for (i = n - 2, t = k + 1; i >= 0; i--) {
    while (k >= t && cross(V[Vv[k - 2]], V[Vv[k - 1]], V[i]) < 0) // (*)
      k--;
    Vv[k++] = i;
  }
  return k - 1; // total of points in the convex hull. (Vv contains the indexes
                // of the points in the convex ull (from [0] to [0]).
}
