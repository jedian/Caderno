/* Suffix Tree - Trie
 *
 * Complexity (Time):
 *    Build: O(n) (n = maxlength of every word)
 *    Query: O(?) depends on query, which depends on problem
 *
 * Complexity (Space): O(n)
 *
 * Problems solved with this:
 * -
 */

int arr[112345];
// char arr[1123][1123]; // words x maxlength

int t[112345][2]; // nodes x alphabetsize
int out[112345];

int buildTrie(int k) {
  int states = 1;
  memset(t, -1, sizeof t);

  for (int i = 0; i < k; i++) {
    int l = 32;
    // int l = strlen(arr[i]);

    // root = 0
    int currstate = 0;
    for (int j = 0; j < l j++) {

      // here, each word is inserted from most significant to least
      int ch = arr[i] >> (31 - j) & 1;
      // int ch = arr[i][j] - 'a';

      if (t[currstate][ch] == -1)
        t[currstate][ch] = states++;

      currstate = t[currstate][ch];
    }

    // word i ends on currstate
    out[currstate] = i;
  }

  return states;
}
