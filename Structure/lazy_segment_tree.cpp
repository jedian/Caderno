/*  Description: Beyond the utilities a simple segment tree provides, a lazy
 *	segment tree support range updates in O(log n)
 *
 *  Utilities: Can be used to RMQ (Range minimum/maximum Query), Sum of
 *  intervals with logarithmic update, etc.

 *  Complexity (Time):
 *      Build   -> O(n log n)
 *      RUpdate -> O(log n)
 *      Query   -> O(log n)

 *  Complexity (Space): O(n)

 * Problems solved with this:
        SEXTA13
        HOMEM-ELEFANTE-RATO
*/

const int MAX = 512345;

int N, tree[2 * MAX], lazy[2 * MAX], arr[MAX];

void build_tree(int node = 1, int a = 0, int b = N) {
  if (a > b)
    return;              // out of range
  if (a == b) {          // is leaf
    tree[node] = arr[a]; // leaf info
    return;
  }
  build_tree(node * 2, a, (a + b) / 2);
  build_tree(node * 2 + 1, 1 + (a + b) / 2, b);
  tree[node] = tree[node * 2] + tree[node * 2 + 1]; // segment info
  // tree[node] = min(tree[node*2], tree[node*2+1]);
}

int modification(int a, int b, int val) {
  // a->b = interval
  return ((b - a) + 1) * val;
}

void update_tree(int i, int j, int val, int node = 1, int a = 0, int b = N) {
  if (lazy[node] != 0) { // this node needs to be updated
    tree[node] += modification(a, b, lazy[node]);
    if (a != b) { // if not leaf, propagates changes
      lazy[node * 2] += lazy[node];
      lazy[node * 2 + 1] += lazy[node];
    }
    lazy[node] = 0;
  }
  if (a > b || a > j || b < i) // out of range
    return;
  if (a >= i && b <= j) { // segment fully within range
    tree[node] += modification(a, b, val);
    if (a != b) {
      lazy[node * 2] += val;
      lazy[node * 2 + 1] += val;
    }
    return;
  }
  update_tree(i, j, node * 2, a, (a + b) / 2);
  update_tree(i, j, node * 2 + 1, 1 + (a + b) / 2, b);
  tree[node] = tree[node * 2] + tree[node * 2 + 1]; // update parents
}

int query_tree(int i, int j, int node = 1, int a = 0,
               int b = N) { // get info [i, j]
  if (a > b || a > j || b < i)
    return 0;            // out of range
  if (lazy[node] != 0) { // this node needs to be updated
    tree[node] += modification(a, b, lazy[node]);
    if (a != b) { // if not leaf, propagates changes
      lazy[node * 2] += lazy[node];
      lazy[node * 2 + 1] += lazy[node];
    }
    lazy[node] = 0;
  }
  if (a >= i && b <= j)
    return tree[node];
  int q1 = query_tree(i, j, node * 2, a, (a + b) / 2);
  int q2 = query_tree(i, j, node * 2 + 1, 1 + (a + b) / 2, b);
  return q1 + q2;
}
