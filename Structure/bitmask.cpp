typedef long long ll;

// Set bit in position pos (0 to 1)
void set(ll &bitmask, int pos) { bitmask |= (1 << pos); }

// Set all bits in a bitmask with size n
void set_all(ll &bitmask, int n) { bitmask = (1 << n) - 1; }

// Unset bit in position pos (1 to 0)
void unset(ll &bitmask, int pos) { bitmask &= ~(1 << pos); }

// Unset all bits
void unset_all(ll &bitmask) { bitmask = 0; }

// Get value of bit in position pos
int get(ll bitmask, int pos) { return bitmask & (1 << pos); }

// Toggle value in position pos
void toggle(ll &bitmask, int pos) { bitmask ^= (1 << pos); }

// Get position of least significant one (Fenwick Tree/BIT)
int least_significant_one(ll bitmask) { return bitmask & (-bitmask); }
