/* Wavelet tree
 *
 * Structure that performs a query for the k'th smallest value in a given
 * interval in O(log n)
 *
 * Complexity (Time):
 *    Query: O(log n)
 *    Build: O(n log n)
 *
 * Complexity (Space): O(n log n)
 *
 * Problems solvable with this:
 *    Batata Quente (URI 2236)
 *
 */

const int MAXN = 1123456;
typedef pair<int, int> ii;

int N;
int nleft[MAXN * 25];
int *tree[MAXN * 25];
ii temp[MAXN], arr[MAXN], sorted[MAXN];
int idx = 0;

int *merge(int e, int d) {
  int *num_left = &nleft[idx];
  idx += d - e + 1;
  int left = e, right = (e + d) / 2 + 1, i = 0, cnt = 0;
  while (left <= (e + d) / 2 && right <= d) {
    if (arr[left].second <= arr[right].second) {
      num_left[i] = ++cnt;
      temp[i] = arr[left++];
    } else {
      num_left[i] = cnt;
      temp[i] = arr[right++];
    }
    i++;
  }
  while (left <= (e + d) / 2) {
    num_left[i] = ++cnt;
    temp[i] = arr[left++];
    i++;
  }
  while (right <= d) {
    num_left[i] = cnt;
    temp[i] = arr[right++];
    i++;
  }
  for (int j = 0; j < (d - e + 1); j++)
    arr[e + j] = temp[j];
  return num_left;
}

// Before calling this, fill sorted array with ii(value, index)(0 -> N-1) and
// sort it. After this, copy sorted to arr and call create_tree();
void create_tree(int i = 1, int e = 1, int d = N) {
  if (e == d)
    return;
  else {
    create_tree(2 * i, e, (e + d) / 2);
    create_tree(2 * i + 1, (e + d) / 2 + 1, d);
    tree[i] = merge(e - 1, d - 1);
  }
}

int query(int p, int q, int k, int i = 1, int st = 1, int end = N) {
  if (st == end)
    return sorted[st - 1].first;
  int left = (p != 1 ? tree[i][p - 2] : 0);
  int right = tree[i][q - 1];
  int diff = right - left;
  if (diff >= k)
    return query(left + 1, right, k, 2 * i, st, st + (end - st) / 2);
  else
    return query(p - left, q - right, k - diff, 2 * i + 1,
                 st + (end - st) / 2 + 1, end);
}
