/*  Description: This data structure is a binary tree that store info of
 *  partial intervals of an array. The kind of information may be the max/min
 *  in that part, sum of it's elements, etc.
 *
 *  Utilities: Can be used to RMQ (Range minimum/maximum Query), Sum of
 *  intervals with logarithmic update, etc.

 *  Complexity (Time):
 *      Build   -> O(n log n)
 *      Update  -> O(log n)
 *      Query   -> O(log n)

 *  Complexity (Space): O(n)

 * Problems solved with this:
        TOPOLAND
*/

const int MAX = 512345;

int N, tree[2 * MAX], arr[MAX];

void build_tree(int node = 1, int a = 0, int b = N) {
  if (a > b)
    return;              // out of range
  if (a == b) {          // is leaf
    tree[node] = arr[a]; // leaf info
    return;
  }
  build_tree(node * 2, a, (a + b) / 2);
  build_tree(node * 2 + 1, 1 + (a + b) / 2, b);
  tree[node] = tree[node * 2] + tree[node * 2 + 1]; // segment info
  // tree[node] = min(tree[node*2], tree[node*2+1]);
}

void update_tree(int idx, int val, int node = 1, int a = 0, int b = N) {
  if (a > b || a > idx || b < idx)
    return;     // out of range
  if (a == b) { // found the pos, edit value
    tree[node] = val;
    return;
  }
  update_tree(idx, val, node * 2, a, (a + b) / 2);
  update_tree(idx, val, node * 2 + 1, 1 + (a + b) / 2, b);
  tree[node] = tree[node * 2] + tree[node * 2 + 1]; // segment info
  // tree[node] = min(tree[node*2], tree[node*2+1]);
}

int query_tree(int i, int j, int node = 1, int a = 0,
               int b = N) { // get info [i, j]
  if (a > b || a > j || b < i)
    return 0; // out of range, null return depending on the query

  // depending on the query and node structure, it is a good idea to have a
  // global array to store answer nodes, then, at the end of query, loop
  // thorugh them to process answer iteratively

  if (a >= i && b <= j) // fully within range
    return tree[node];

  int res1 = query_tree(i, j, node * 2, a, (a + b) / 2);
  int res2 = query_tree(i, j, node * 2 + 1, 1 + (a + b) / 2, b);
  return res1 + res2;
  // return min(res1, res2);
}
