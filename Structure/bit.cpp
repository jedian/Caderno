/* Description: This data structure is a binary tree for implementing dynamic
 * cumulative frequency tables.
 *
 * Utilities: It solves the problem of building a cumulative frequency table for
 * each update in the array, performing only O(log n) updates rather than O(n).
 *
 * Complexity (Time):
 *   Update -> O(log n)
 *   Query  -> O(log n)
 *
 * Complexity (Space): O(n)
 *
 * Problems solved with this:
 *   High Five
 *   BALE11
*/

int tree[MAX];

// Perform query in array (tree) in the idx position
int query(int idx) {
  int sum = 0;
  for (; idx > 0; idx -= (idx & -idx))
    sum += tree[idx];

  return sum;
}

// Add a value (val) to a single position (idx) in the array (tree).
void update(int idx, int val) {
  for (; idx <= MAX; idx += (idx & -idx))
    tree[idx] += val;
}

/* --- 2D version
 *
 * Complexity (Time):
 *   Update -> O(log^2 n)
 *   Query  -> O(log^2 n)
 *
 * Complexity (Space): O(n^2)
 */

int query(int idx, int idy) {
  int sum = 0, m;
  for (; idx > 0; idx -= (idx & -idx))
    for (m = idy; m > 0; m -= (m & -m))
      sum += tree[idx][m];

  return sum;
}

void update(int idx, int idy, int val) {
  int m;
  for (; idx <= MAX; idx += (idx & -idx))
    for (m = idy; m <= MAX; m += (m & -m))
      tree[idx][m] += val;
}
