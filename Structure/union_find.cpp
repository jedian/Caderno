/* Complexity (Time):
 *    Every method: O(1)

 * Complexity (Space): O(n)

 * Problems solved with this:
 *    Labirintos de cerca viva
 *    Minimum spanning tree
*/
const int MAX;

int parent[MAX];
int rank[MAX];
int sz[MAX];

void makeset(int x) {
  parent[x] = x;
  rank[x] = 0;
  sz[x] = 1; // size of the set x is contained
}

int find(int x) {
  if (parent[x] != x)
    parent[x] = find(parent[x]);
  return parent[x];
}

void unionset(int x, int y) {
  int xroot = find(x);
  int yroot = find(y);

  if (xroot == yroot)
    return;

  if (rank[xroot] < rank[yroot]) {
    parent[xroot] = yroot;
    sz[yroot] += sz[xroot];
  } else if (rank[xroot] > rank[yroot]) {
    parent[yroot] = xroot;
    sz[xroot] += sz[yroot];
  } else {
    parent[yroot] = xroot;
    sz[xroot] += sz[yroot];
    rank[xroot]++;
  }
}
