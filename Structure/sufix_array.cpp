/* Description: supports fast sufix queries, and string sorting.
 *
 *  Complexity (TIME):
 *      build -> O(n log n log n)
 *      query -> O(log n)
 *
 *  Complexity (SPACE): O(n)
 *
 *  Problem solved with:
 *      I love strings
 */

#define MAX 101010 // (n)

char S[MAX]; // base string
char T[MAX]; // sufix query

int ids[MAX];     // actual index array
int bucket[MAX];  // as in bucket sort
int nBucket[MAX]; // used to keep bucket intact while refreshing
int seg_size;     // Actual sorting segment size

// compair strings of size seg_size on O(1)
bool cmp(int a, int b) {
  if (seg_size == 1)
    return S[a] < S[b];
  if (bucket[a] == bucket[b])
    return bucket[a + (seg_size / 2)] < bucket[b + (seg_size / 2)];
  return bucket[a] < bucket[b];
}

void build() {
  int idx = 0;
  int sz = strlen(S);

  for (int i = 0; i <= sz; i++)
    ids[i] = i;

  for (seg_size = 1; seg_size < sz; seg_size *= 2) {
    sort(ids, ids + sz + 1, cmp);
    int k = 0;
    // refresh buckets
    for (int i = 0; i <= sz; i++) {
      nBucket[ids[i]] = k;
      if (i < sz && cmp(ids[i], ids[i + 1]))
        k++;
    }
    for (int i = 0; i <= sz; i++)
      bucket[i] = nBucket[i];
  }
}

// binary search
bool query() {
  int b = 0, e = strlen(S), m;
  int szT = strlen(T);
  for (int i = 0; i < 81; i++) {
    m = b + ((e - b) / 2);
    int res = strncmp(T, S + ids[m], szT);
    if (res > 0) {
      b = m;
    } else if (res < 0) {
      e = m;
    } else {
      return true;
    }
  }
  return false;
}
