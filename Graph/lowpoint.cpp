/* Finds bridges and articulation points in a graph
 *
 * Complexity (Time): O(|E| + |V|)
 *
 * Complexity (Space): O(|E| + |V|)
 *
 * Problems solved with this:
 *   Labirintos de Cerca Viva
 *   Submerging Islands (SUBMERGE)
*/

vector<int> graph[MAX];

int cont[MAX], parent[MAX];
int low[MAX], L[MAX];
bool is_artic[MAX];
bool is_bridge[MAX][MAX];

void dfs(int x) {
  cont[x] = 1;
  int child = 0;

  for (auto i : graph[x]) {
    if (!cont[i]) {
      child++;
      parent[i] = x;

      low[i] = L[i] = L[x] + 1;
      dfs(i);
      low[x] = min(low[x], low[i]);

      // If parent[x] is -1 then x is the root of the search, so if x has more
      // than one child it is an articulation point.
      // Otherwise it is an articulation only if low[i] >= L[x]
      if ((parent[x] == -1 && child > 1) || (parent[x] != -1 && low[i] >= L[x]))
        // CAUTION: may be executed more than once for the same vertex
        is_artic[x] = true;

      if (low[i] > L[x])
        // Can be stored in a better way
        is_bridge[x][i] = is_bridge[i][x] = true;

    } else if (parent[x] != i)
      low[x] = min(low[x], L[i]);
  }
}
