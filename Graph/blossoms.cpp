// ALGORITMO DE EDMONS PARA MATCHING EM GRAFO NAO-BIPARTIDO.
// ENQT PROCURA CAMINHOS AUMENTANTES, ENCONTRA BLOSSONS (CICLOS IMPARES),
// COMPACTA ELE NUM VERTICE SOH E TENTA DENOVO. A WIKI DIZ Q EH O(N^4). PASSOU
// MUITO APERTADO NO SPOJ COM N=100, NAO USAR NA LOKA!
// PASSA A MATRIZ DE ADJ POR PARAMENTRO, CUIDADO!

// visited eh int e NAO EH por TS, eh pq o algoritmo requer mais valores msm.
int n, par[128], visited[128];

inline void emparelha(int a, int b) {
  par[a] = b;
  par[b] = a;
}

// dfs que retorna true se achar um blossom ou um caminho aumentante.
bool dfs(int u, vector<vector<bool>> &M, vector<int> &blossom) {
  visited[u] = 0;
  for (int i = 0; i < n; i++)
    if (M[u][i]) {
      if (visited[i] == -1) {
        visited[i] = 1;
        if (par[i] == -1 or dfs(par[i], M, blossom)) {
          emparelha(u, i);
          return true;
        }
      }
      if (visited[i] == 0 or !blossom.empty()) { // achei uma fro
        blossom.push_back(i);
        blossom.push_back(u);
        if (u == blossom[0]) {
          par[u] = -1;
          return true;
        }
        return false;
      }
    }
  return false;
}

// busca um caminho aumentante. Se achar um blossom, compacta os vertices e
// tenta achar caminho devolta. Passa a matriz de adjacencia como paramentro.
// never do that again!
bool temcaminho(vector<vector<bool>> &M) {
  for (int u = 0; u < n; u++)
    if (par[u] == -1) {
      vector<int> blossom;
      memset(visited, 0xff, n * sizeof(int));
      if (!dfs(u, M, blossom))
        continue; // nada d+? entao dane-se
      if (blossom.empty())
        return true; // axei um caminho aumentante [ja emparelhei no dfs]

      int B = (int)blossom.size();
      int base = blossom[0]; // ou blossom[B-1], sao iguais!
      // achei um blossom. compacta!
      vector<vector<bool>> nM = M;
      // Qm eh vizinho de um blossom eh vizinho da base.
      // [a base vai ser o blossom]
      for (int i = 1; i < B - 1; i++)
        for (int j = 0; j < n; j++)
          if (M[blossom[i]][j])
            nM[base][j] = nM[j][base] = true;
      // arranca fora qm nao eh base
      for (int i = 1; i < B - 1; i++)
        for (int j = 0; j < n; j++)
          nM[blossom[i]][j] = nM[j][blossom[i]] = false;
      nM[base][base] = false;
      // tenta devolta
      if (!temcaminho(nM))
        return false;
      // foi! emparelha dentro do blossom
      int u = par[base];
      if (u != -1)
        for (int i = 0; i < B; i++)
          if (M[blossom[i]][u]) {
            emparelha(blossom[i], u);
            if (i & 1)
              for (int j = i + 1; j < B; j += 2)
                emparelha(blossom[j], blossom[j + 1]);
            else
              for (int j = 0; j < i; j += 2)
                emparelha(blossom[j], blossom[j + 1]);
            break;
          }
      return true;
    }
  return false;
}

// matching nao bipartido. retorna o numero de pares matcheados
// (sao 2*r nodos com par). O par de cada nodo fica em par[i]
int maxmatch(vector<vector<bool>> &M) {
  int r = 0;
  memset(par, 0xff, n * sizeof(int));
  while (temcaminho(M))
    r++;
  return r;
}
