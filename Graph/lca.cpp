/* Complexity (Time):
 *   Setup -> O(n)
 *   Query -> O(sqrt(n))
 *
 * Complexity (Space): O(n + m)
 *
 * Problems solved with this:
 *   Colonia de Formigas
*/

vector<int> graph[MAX];

// Height of each vertex
int h[MAX];

// Parent of each vertex
int par[MAX];

// Return index of the lowest common ancestor between
// u and v
int lca(int u, int v) {
  while (u != v) {
    if (h[u] == h[v]) {
      u = par[u];
      v = par[v];
    } else if (h[u] > h[v]) {
      u = par[u];
    } else {
      v = par[v];
      return u;
    }
  }
}

// Setup. Should be executed only once before queries,
// x is the root of the tree
void bfs(int x) {
  memset(h, -1, sizeof h);
  memset(par, -1, sizeof par);

  queue<int> Q;
  Q.push(x);

  while (!Q.empty()) {
    x = Q.front();
    Q.pop();

    for (auto v : graph[x])
      if (h[v] == -1) {
        par[v] = x;
        h[v] = h[x] + 1;
        Q.push(v);
      }
  }
}
