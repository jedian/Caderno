/* Floyd Warshall Algorithm
 *
 * Complexity (Time): O(n^3)
 *
 * Complexity (Space): O(n^2)
 *
 * Problems solved with this:
 *   Final Mundial de 2008
 */

// Adjacency matrix
int graph[MAX][MAX];

// Shortest distance matrix
int dist[MAX][MAX];

int floyd_warshall(int n) {
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      dist[i][j] = graph[i][j];

  for (int k = 0; k < n; ++k)
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        if (dist[i][k] + dist[k][j] < dist[i][j])
          dist[i][j] = dist[i][k] + dist[k][j];
}
