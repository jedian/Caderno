/* Kruskal for MST
 *
 * Complexity (Time): O(m log n)
 *
 * Complexity (Space): O(m)
 *
 * Problems solved with this:
 *    Roteadores (URI1774)
 *    Rede do DINF
 */

const int MAXN = 1000;
typedef pair<int, pair<int, double>> iid;
vector<iid> Edg;
int parent[MAXN];

// < | minimum spanning tree
// > | maximum spanning tree
bool cmp(iid a, iid b) { return a.second.second < b.second.second; }

int findset(int x) {
  if (x != parent[x])
    parent[x] = findset(parent[x]);
  return parent[x];
}

double kruskal() {
  list<iid> T; // Final spanning tree
  sort(Edg.begin(), Edg.end(), cmp);
  int i, pu, pv;
  double size = 0.0;
  for (int i = 0; i < MAXN; i++) {
    parent[i] = i;
  }
  for (int i = 0; i < Edg.size(); i++) {
    pu = findset(Edg[i].first);
    pv = findset(Edg[i].second.first);
    if (pu != pv) {
      T.push_back(Edg[i]); // Add edge to spanning tree;
      size += Edg[i].second.second;
      parent[pu] = parent[pv];
    }
  }
  return size; // return sum of all edge's weight in tree.
}
