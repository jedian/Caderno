/* Bellman ford for shortest path with negative cost edges
 *
 * Complexity (Time): O(V * E)
 *
 * Complexity (Space): O(V + E)
 *
 * Problems solved with this:
 *    Haunted Graveyard
 */

const int MAXN = 1123;
typedef long long ll;
ll dist[MAXN], pred[MAXN];

typedef struct { int s, t, w; } edge;

vector<edge> E;

ll bellmanford(int s, int t, int n) {
  int e = E.size();
  memset(dist, 0x3f, sizeof(dist));
  memset(pred, -1, sizeof(pred));
  dist[s] = 0;
  for (int i = 0; i < n - 1; i++) {
    for (int j = 0; j < e; j++) {
      edge &aux = E[j];
      if (dist[aux.s] + aux.w < dist[aux.t]) {
        dist[aux.t] = dist[aux.s] + aux.w;
        pred[aux.t] = aux.s;
      }
    }
  }
  for (int j = 0; j < e; j++) {
    edge &aux = E[j];
    if (dist[aux.s] + aux.w < dist[aux.t]) {
      // negative cycle
      return -0x3f3f3f3f3f3f3f3fLL;
    }
  }
  return dist[t];
}
