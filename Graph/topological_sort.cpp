/* Topological Sort
 *
 * Complexity (Time): O(n + m)
 *
 * Complexity (Space): O(n + m)
 *
 * Problems solved with this:
 *    Manyfile
 */

stack<int> S;
vector<int> graph[MAX];

bool cont[MAX];
bool cycle = false;

int dfs(int x) {
  cont[x] = 1;

  for (auto i : graph[x])
    if (!cycle && !cont[i])
      dfs(i);
    else
      cycle = true;

  S.push(x);
}

// This function returns a linear ordering of the vertices in graphs such that
// for every directed edge (u, v), the vertex u comes before v.
vector<int> topological_sort() {
  memset(cont, 0, sizeof(cont));

  for (int i = 0; i < MAX; ++i)
    if (!cont[i])
      dfs(i);

  vector<int> top_sort;

  if (!cycle)
    while (!S.empty()) {
      top_sort.push_back(S.top());
      S.pop();
    }

  return top_sort;
}
