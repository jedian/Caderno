/* Dinic for max flow
 *
 * Complexity (Time): O(V^2 * E)
 *
 * Complexity (Space): O(V^2)
 *
 * Problems solved with this:
 *    A lei vai a cavalos.
 */

const int MAXN = 305;
int cap[MAXN][MAXN], flo[MAXN][MAXN], s, t, d[MAXN], ptr[MAXN], q[MAXN];
int cav[200];

bool bfs() {
  int qh = 0, qt = 0;
  q[qt++] = s;
  memset(d, -1, sizeof(d));
  d[s] = 0;
  while (qh < qt) {
    int v = q[qh++];
    for (int to = 0; to < MAXN; ++to)
      if (d[to] == -1 && flo[v][to] < cap[v][to]) {
        q[qt++] = to;
        d[to] = d[v] + 1;
      }
  }
  return d[t] != -1;
}

int dfs(int v, int flow) {
  if (!flow)
    return 0;
  if (v == t)
    return flow;
  for (int &to = ptr[v]; to < MAXN; ++to) {
    if (d[to] != d[v] + 1)
      continue;
    int pushed = dfs(to, min(flow, cap[v][to] - flo[v][to]));
    if (pushed) {
      flo[v][to] += pushed;
      flo[to][v] -= pushed;
      return pushed;
    }
  }
  return 0;
}

int dinic() { // initialize s and t before call
  int flow = 0;
  memset(flo, 0, sizeof(flo));
  for (;;) {
    if (!bfs())
      break;
    memset(ptr, 0, sizeof(ptr));
    while (int pushed = dfs(s, 0x3f3f3f3f))
      flow += pushed;
  }
  return flow;
}
