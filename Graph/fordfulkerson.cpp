/* Ford- Fulkerson for max flow in graphs.
 *
 * Complexity (Time): O(E*f)
 *
 * Complexity (Space): O(E + V)
 *
 * Problems solved with this:
 *    CAVALOS (Spoj)
 */

const int MAX = 300;
typedef struct edge { int u, v, w, n, c, f; } edge;

edge list[MAX * MAX]; // start with -1
bool vis[MAX];
int header[MAX]; // first edge of each node, start with -1
int H = 0;       // this must be 0 for every new graph

void addEdge(int u, int v, int w, int c,
             bool bi) { // u, v, weight, capacity, bidirected?
  list[H] = (edge){u, v, w, header[u], c, 0};
  header[u] = H++;
  // if not bidirected flow, need reverse edge with 0 capacity
  if (bi)
    list[H] = (edge){v, u, w, header[v], 0, 0}, header[v] = H++;
  else
    H++;
}

int dfs(int u, int t, int cap = 0x3f3f3f3f) {
  vis[u] = 1;
  if (u == t)
    return cap;
  for (int q = header[u]; q != -1; q = list[q].n)
    if ((list[q].c - list[q].f) > 0 && !vis[list[q].v]) {
      int f = dfs(list[q].v, t, min(cap, list[q].c - list[q].f));
      if (f > 0) {
        list[q].f += f;
        list[q ^ 1].f -= f;
        return f;
      }
    }
  return -1;
}
// needs bfs for edmonds-karp
int fordfulkerson(int s, int t) {
  int flow = 0, f;
  memset(vis, 0, sizeof(vis));
  while ((f = dfs(s, t)) > 0) {
    memset(vis, 0, sizeof(vis));
    flow += f;
  }
  return flow;
}
