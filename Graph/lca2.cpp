/* Complexity in time:
 *      Preprocess -> 0(n log n)
 *      Query -> O(log n)
 */

const int MAX = 1123456;
typedef pair<int, int> ii;
vector<ii> adj[MAX];
int P[MAX][100];
int L[MAX]; // level in tree (dfs)
int T[MAX]; // father in tree(dfs)
int val[MAX];
bool vis[MAX];

void dfs(int n, int f, int l, int c) {
  val[n] = c;
  T[n] = f;
  L[n] = l;
  for (auto v : adj[n]) {
    if (!vis[v.first]) {
      vis[v.first] = 1;
      dfs(v.first, n, l + 1, v.second);
    }
  }
}

void preprocess(int N) {
  int i, j;
  memset(vis, 0, sizeof(vis));
  L[1] = 0;
  vis[1] = 1;
  val[1] = 0;
  dfs(1, 1, 0, 0);

  for (i = 0; i < N; i++)
    for (j = 0; (1 << j) < N; j++) {
      P[i][j] = -1;
      // cost[i][j] = -1;
    }

  for (i = 0; i < N; i++) {
    P[i][0] = T[i];
    // cost[i][0] = val[i];
  }

  for (j = 1; (1 << j) < N; j++)
    for (i = 0; i < N; i++)
      if (P[i][j - 1] != -1) {
        P[i][j] = P[P[i][j - 1]][j - 1];
        // cost[i][j] = max(cost[i][j], max(cost[P[i][j-1]][j-1],
        // cost[i][j-1]));
        // cost[i][j] += cost[i][j-1] + cost[P[i][j-1]][j-1];
      }
}

int query(int p, int q) {
  int tmp, log, i;
  // int ans = 0;

  if (L[p] < L[q]) {
    tmp = p;
    p = q;
    q = tmp;
  }
  for (log = 1; (1 << log) <= L[p]; log++)
    ;
  log--;
  for (i = log; i >= 0; i--)
    if ((L[p] - (1 << i)) >= L[q]) {
      // ans = max(ans, cost[p][i]);
      // ans += cost[p][i];
      p = P[p][i];
    }

  if (p == q)
    return p; // ans
  for (i = log; i >= 0; i--)
    if ((P[p][i] != -1) && P[p][i] != P[q][i]) {
      // ans = max(ans, max(cost[p][i], cost[q][i]));
      // ans += cost[p][i] + cost[q][i];
      p = P[p][i];
      q = P[q][i];
    }
  // if(p==q) return ans;
  // else return max(max(ans, val[p]), val[q]);
  // else return ans + cost[p] + cost[q];
  return T[p];
}
