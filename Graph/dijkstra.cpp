/* Description: Graph algorithm.
 *
 * Utilities: This algorithm solves the problem of finding the shortest path
 * from a source to every vertex in a weighted graph.
 *
 * Complexity (Time):
 *   Execution -> O(m + n log n), m = |E|, n = |V|
 *
 * Complexity (Space): O(m + n)
 *
 * Problems solved with this:
 *   Viagem Para BH
 *   ENGARRAF
*/

#define oo 0x3f3f3f3f
typedef pair<int, int> ii;

// Adjacency list where the path between A and B with weight W is added by
// graph[A].push_back(ii(B, W))
vector<ii> graph[MAX];

// Array of distance. The distance between the origin and I is stored
// at dist[I]
int dist[MAX];

// Perform Dijkstra's algorithm for calculating the distance between o
// and every other vertex. Return the distance between o and d
int dijkstra(int o, int d) {
  set<ii> pq;
  set<ii>::iterator it;
  int u, v, wt;

  // Set every distance to infinity
  memset(dist, oo, sizeof dist);

  // Set the distance between o and o, which is zero
  dist[o] = 0;
  pq.insert(make_pair(0, o));

  while (pq.size() != 0) {
    // Get the first element of the priority queue (the one with the
    // shortest distance so far)
    it = pq.begin();
    u = it->second;
    pq.erase(it);

    // Iterate neighbors of u
    for (auto i : graph[u]) {
      v = i.first;
      wt = i.second;

      // Update distance if a shortest one has been found
      if (dist[v] > dist[u] + wt) {
        if (dist[v] != oo)
          pq.erase(pq.find(make_pair(dist[v], v)));

        dist[v] = dist[u] + wt;

        // Update the value of v in the priority queue
        pq.insert(make_pair(dist[v], v));
      }
    }
  }

  return dist[d];
}
