/* Augmenting Paths for Bipartite Matching
 *
 * Complexity (Time): ~O(n) with greedy start approach
 *
 * Complexity (Space): O(n)
 *
 * Problems solved with this:
 *    Nuts and Bolts (UVA)
 */
const int MAX = 1123;

int vis[MAX], match[MAX], l, r;
vector<int> adj[502];

int aug(int u) {
  if (vis[u])
    return 0;
  vis[u] = 1;
  for (auto v : adj[u]) {
    if (match[v] == -1 || aug(match[v])) {
      match[v] = u;
      return 1;
    }
  }
  return 0;
}

int augmentingpath(int n) {
  int ans, i;
  memset(match, -1, sizeof(match));
  for (ans = i = 0; i < n; i++) {
    memset(vis, 0, sizeof(vis));
    ans += aug(i);
  }
  return ans; // total matched
}
