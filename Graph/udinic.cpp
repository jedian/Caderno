/* Ultra Dinic for max flow
 *
 * Complexity (Time): O(V^2 * E)
 *
 * Complexity (Space): O(E + V)
 *
 * Problems solved with this:
 *    A lei vai a cavalos.
 *    Fastflow (SPOJ.com)
 */
const int MAXN = 5123;
int s, t, d[MAXN], ptr[MAXN], q[MAXN];
typedef long long int lli;
typedef struct edge { int u, v, w, n, c, f; } edge;

edge lista[312345];
int header[MAXN];
int H = 0;

void addEdge(int u, int v, int w, int c, bool bi) {
  lista[H] = (edge){u, v, w, header[u], c, 0};
  header[u] = H++;
  // if edges are bidirectional, v-u edge.cap is c, else is 0 just for residual
  // graph
  if (bi)
    lista[H] = (edge){v, u, w, header[v], c, 0}, header[v] = H++;
  else
    H++;
}

bool bfs() {
  int qh = 0, qt = 0, to;
  q[qt++] = s;
  memset(d, -1, sizeof(d));
  d[s] = 0;
  while (qh < qt) {
    int v = q[qh++];
    for (int qq = header[v]; qq != -1; qq = lista[qq].n) {
      to = lista[qq].v;
      if (d[to] == -1 && lista[qq].f < lista[qq].c) {
        q[qt++] = to;
        d[to] = d[v] + 1;
      }
    }
  }
  return d[t] != -1;
}

int dfs(int v, int flow) {
  int to;
  if (!flow)
    return 0;
  if (v == t)
    return flow;
  for (int &q = ptr[v]; q != -1; q = lista[q].n) {
    to = lista[q].v;
    if (d[to] != d[v] + 1)
      continue;
    int pushed = dfs(to, min(flow, lista[q].c - lista[q].f));
    if (pushed) {
      lista[q].f += pushed;
      lista[q ^ 1].f -= pushed;
      return pushed;
    }
  }
  return 0;
}

lli dinic() { // initialize s and t before call
  lli flow = 0;
  for (;;) {
    if (!bfs())
      break;
    for (int i = 0; i < MAXN; i++)
      ptr[i] = header[i];
    while (int pushed = dfs(s, 0x3f3f3f3f))
      flow += pushed;
  }
  return flow;
}
